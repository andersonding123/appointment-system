<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Doctor\DashboardController as DoctorDashboard;
use App\Http\Controllers\Doctor\LoginController as DoctorLogin;
use App\Http\Controllers\Doctor\AppointmentController as DoctorAppointment;

// doctors
Route::middleware(['guest:doctor'])->group(function () {
    Route::get('login', [DoctorLogin::class, 'showLoginForm'])->name('login');
    Route::post('login', [DoctorLogin::class, 'login'])->name('login_request');
});
Route::middleware(['auth:doctor'])->group(function () {
    Route::post('logout', [DoctorLogin::class, 'logout'])->name('logout');
    Route::match(['get','post'], 'dashboard', [DoctorDashboard::class, 'index'])->name('dashboard');
    Route::match(['get','post'], 'appointment', [DoctorAppointment::class, 'index'])->name('appointment');
    Route::post('appointment/complete', [DoctorAppointment::class, 'complete_appointment'])->name('complete_appointment');
});