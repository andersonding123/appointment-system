<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Nurse\DashboardController as NurseDashboard;
use App\Http\Controllers\Nurse\LoginController as NurseLogin;
use App\Http\Controllers\Nurse\AppointmentController as NurseAppointment;

// nurses
Route::middleware(['guest:nurse'])->group(function () {
    Route::get('login', [NurseLogin::class, 'showLoginForm'])->name('login');
    Route::post('login', [NurseLogin::class, 'login'])->name('login_request');
});
Route::middleware(['auth:nurse'])->group(function () {
    Route::post('logout', [NurseLogin::class, 'logout'])->name('logout');
    Route::match(['get','post'], 'dashboard', [NurseDashboard::class, 'index'])->name('dashboard');
    Route::match(['get','post'], 'appointment', [NurseAppointment::class, 'index'])->name('appointment');
    Route::match(['get','post'], 'appointment/assign/{id}', [NurseAppointment::class, 'assign_appointment'])->name('assign_appointment');
    Route::post('appointment/complete', [NurseAppointment::class, 'complete_appointment'])->name('complete_appointment');
});