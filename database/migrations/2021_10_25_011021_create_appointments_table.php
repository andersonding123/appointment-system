<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id('appointment_id');
            $table->string('appointment_title');
            $table->text('appointment_desc');
            $table->dateTime('appointment_date', $precision = 0);
            $table->enum('appointment_status', ['pending', 'done']);
            $table->string('appointment_code');
            $table->foreignId('patient_id');
            $table->foreignId('nurse_id');
            $table->foreignId('doctor_id');
            $table->foreignId('department_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
