<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\NurseSeeder;
use Database\Seeders\DoctorSeeder;
use Database\Seeders\DepartmentSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            NurseSeeder::class,
            DoctorSeeder::class,
            DepartmentSeeder::class
        ]);
    }
}
