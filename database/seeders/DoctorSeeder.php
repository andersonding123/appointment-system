<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doctors')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10)."@gmail.com",
            'mobile' => "601".Str::random(9),
            'login_id' => "D".Str::random(5),
            'password' => Hash::make('password'),
            'department_id' => rand(1,4)
        ]);
    }
}
