<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class NurseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nurses')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10)."@gmail.com",
            'mobile' => "601".Str::random(9),
            'login_id' => "N".Str::random(5),
            'password' => Hash::make('password')
        ]);
    }
}
