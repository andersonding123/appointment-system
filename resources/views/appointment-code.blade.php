@extends('layouts.nologin-app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Appointment</div>

                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-12 text-center">
                            <label for="name" class="col-form-label">Your Appointment Code</label>
                            <h5><b>{{ $code }}</b></h5>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 text-center">
                            <a href="{{ route('homepage') }}" class="btn btn-primary">
                                Back to Home
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection