@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Appointment</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{ route('doctor.appointment') }}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label for="freetext">Freetext</label>
                                <input type="text" class="form-control" name="freetext" id="freetext" value="{{ $search }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <button type="submit" name="form-submit" class="btn btn-primary" value="search">Submit</button>
                                <button type="submit" name="form-submit" class="btn btn-danger" value="reset">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Patient</th>
                                    <th>Illness Description</th>
                                    <th>Appointment Date</th>
                                    <th>Appointment Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($records->isNotEmpty())
                                    @php 
                                        $cnt = 1;
                                    @endphp
                                    @foreach($records as $val)
                                        <tr>
                                            <td>{{ $cnt }}</td>
                                            <td>
                                                <b>{{ $val->patients->name }}</b> <br>
                                                {{ $val->patients->dob }}
                                            </td>
                                            <td>
                                                <b>{{ $val->appointment_title }}</b> <br>
                                                {{ $val->appointment_desc }}
                                            </td>
                                            <td>{{ date('Y-m-d',strtotime($val->appointment_date)) }}</td>
                                            <td>{{ Str::ucfirst($val->appointment_status) }}</td>
                                            <td>
                                                @if($val->appointment_status=='pending')
                                                    <a data-id="{{ $val->appointment_id }}" class="complete-appointment btn btn-sm btn-primary" href="javascript:void(0)">Complete</a>
                                                @endif
                                            </td>
                                        </tr>
                                        @php 
                                            $cnt++;
                                        @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">No record found!</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            {!! $records->links() !!}
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $(".complete-appointment").click(function(){
            var id = $(this).data("id");
            $.ajax({
                type: "POST",
                url: "{{ route('doctor.complete_appointment') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
                dataType: "json",
                success: function(e){
                    console.log(e);
                    if(e.status){
                        window.location.href="{{ route('doctor.appointment') }}";
                    }
                },
                error: function(){
                    console.log("Error submitting!");
                }
            });
        });
    });
</script>
@endsection