@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Appointment</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if($errors)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                        @endforeach
                    @endif

                    <form action="{{ route('nurse.assign_appointment',['id'=>$record->appointment_id]) }}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label><b>Department:</b> </label>
                                <span>{{ $record->departments->department_name }}</span>
                            </div>
                            <div class="col-md-12 mt-2">
                                <label><b>Patient:</b> </label>
                                <span>{{ $record->patients->name }}</span>
                            </div>
                            <div class="col-md-12 mt-2">
                                <label><b>Details:</b> </label>
                                <div>
                                    {{ $record->appointment_title }} <br>
                                    {{ $record->appointment_desc }}
                                </div>
                            </div>
                            <div class="col-md-6 mt-2">
                                <label for="doctor_id"><b>Assign a doctor:</b> </label>
                                <select id="doctor_id" name="doctor_id" class="form-control">
                                    @foreach($available_doctors as $key => $val)
                                        <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
