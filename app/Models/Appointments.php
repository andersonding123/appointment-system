<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointments extends Model
{
    use HasFactory;

    protected $table = 'appointments';
    protected $primaryKey = 'appointment_id';
    protected $fillable = [
        'appointment_title', 'appointment_desc', 'appointment_date', 'appointment_code', 'appointment_status', 'patient_id', 'nurse_id', 'doctor_id', 'department_id'
    ];

    public function patients(){
        return $this->belongsTo('App\Models\Patients','patient_id');
    }

    public function doctors(){
        return $this->belongsTo('App\Models\Doctors','doctor_id');
    }

    public function nurses(){
        return $this->belongsTo('App\Models\Nurses','nurse_id');
    }

    public function departments(){
        return $this->belongsTo('App\Models\Departments','department_id');
    }

    // retrieve
    public static function get_records($appointment_code, $perpage){
        $query = Appointments::query();

        if(!empty($appointment_code)){
            $query->where('appointment_code',$appointment_code);
        }

        return $query->paginate($perpage);
    }

    public static function get_doctor_appointment($freetext,$doctor_id,$perpage){
        $query = Appointments::query();

        if(!empty($freetext)){
            $query->where(function($q) use($freetext){
                $q->orWhere('appointment_title','like','%'.$freetext.'%')
                    ->orWhere('appointment_desc','like','%'.$freetext.'%');
            });
        }

        return $query->where('doctor_id',$doctor_id)->paginate($perpage);
    }
}
