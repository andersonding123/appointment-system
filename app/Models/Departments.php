<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    use HasFactory;

    protected $table = 'departments';
    protected $primaryKey = 'department_id';
    protected $fillable = [
        'department_name', 'department_desc'
    ];

    public function appointments(){
        return $this->hasMany('App\Models\Appointments','department_id');
    }

    public static function get_selection()
    {
        $options[""] = "Please select a department";
        $query = Departments::query()->select('department_id','department_name')->get();
        if($query->isNotEmpty()){
            foreach($query as $value){
                $options[$value->department_id] = $value->department_name;
            }
        }
        return $options;
    }

    public function run(){
        Departments::factory()
                ->count(4)
                ->create();
    }
}
