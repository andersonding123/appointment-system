<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Doctors extends Authenticatable
{
    use HasFactory;

    protected $table = 'doctors';
    protected $primaryKey = 'doctor_id';
    protected $fillable = [
        'name', 'email', 'mobile', 'login_id', 'password', 'department_id'
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function appointments(){
        return $this->hasMany('App\Models\Appointments','doctor_id');
    }

    // retrieve
    public static function get_department_doctors_sel($department_id){
        $options[""] = "Select a doctor";
        $query = Doctors::query()->select('doctor_id','name')->where('department_id',$department_id)->get();
        if($query->isNotEmpty()){
            foreach($query as $val){
                $options[$val->doctor_id] = $val->name;
            }
        }
        return $options;
    }

    public function run(){
        Doctors::factory()
                ->count(10)
                ->create();
    }
}
