<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Nurses extends Authenticatable
{
    use HasFactory;

    protected $table = 'nurses';
    protected $primaryKey = 'nurse_id';
    protected $fillable = [
        'name', 'email', 'mobile', 'login_id', 'password'
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function appointments(){
        return $this->hasMany('App\Models\Appointments','nurse_id');
    }

    public function run(){
        Nurses::factory()
                ->count(10)
                ->create();
    }
}
