<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    use HasFactory;

    protected $table = 'patients';
    protected $primaryKey = 'patient_id';
    protected $fillable = [
        'name', 'email', 'mobile', 'dob' // 'password'
    ];

    public function appointments(){
        return $this->hasMany('App\Models\Appointments','patient_id');
    }

    public static function get_existing_by_email($email){
        $query = Patients::query()->where('email',$email)->first();
        return $query;
    }
}
