<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Models\Departments;
use App\Models\Patients;
use App\Models\Appointments;
use App\Mail\CodeMail;
use Illuminate\Support\Facades\Mail;

class AppointmentController extends Controller
{
    public function index(Request $request)
    {
        $validator = null;

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'email' => 'required|email',
                'mobile' => 'required|numeric',
                'department' => 'required',
                'title' => 'required',
                'description' => 'required',
                'appointment_date' => 'required' 
            ])->setAttributeNames([
                'name' => 'Name',
                'email' => 'Email',
                'mobile' => 'Mobile',
                'department' => 'Department',
                'title' => 'Title',
                'description' => 'Description',
                'appointment_date' => 'Appointment Date' 
            ]);

            if(!$validator->fails()){
                $patient = Patients::get_existing_by_email($request->input('email'));
                if($patient){
                    $patient_id = $patient->patient_id;
                }
                else{
                    $mobile = $request->input('mobile');
                    if(substr($mobile,0,1)==0){
                        $mobile = "6".$mobile;
                    }
                    elseif(substr($mobile,0,1)==1){
                        $mobile = "60".$mobile;
                    }
                    $patient_id = Patients::insertGetId([
                        'name' => $request->input('name'),
                        'email' => $request->input('email'),
                        'mobile' => $mobile,
                        'dob' => ''
                    ]);
                }

                $code = rand();

                Appointments::create([
                    'appointment_title' => $request->input('title'),
                    'appointment_desc' => $request->input('description'),
                    'appointment_date' => date('Y-m-d H:i:s',strtotime($request->input('appointment_date'))),
                    'appointment_status' => 'pending',
                    'appointment_code' => $code,
                    'patient_id' => $patient_id,
                    'nurse_id' => 0,
                    'doctor_id' => 0,
                    'department_id' => $request->input('department')
                ]);

                Session::put('code', $code);

                $data['code'] = $code;
                Mail::send('mail-code', $data, function ($message) use($request) {
                    $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->to($request->email, $request->name);
                    $message->subject('Appointment Code');
                });

                return redirect()->route('appointment.code');
            }
        }

        return view('appointment',[
            'departments' => Departments::get_selection()
        ])->withErrors($validator);
    }

    public function code(Request $request)
    {
        $code = "";

        if(Session::has('code')){
            $code = Session::get('code');
        }

        if(empty($code)){
            return redirect('/');
        }

        return view('appointment-code',[
            'code' => $code
        ]);
    }
}
