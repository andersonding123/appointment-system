<?php

namespace App\Http\Controllers\Nurse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:nurse')->except('logout');
    }

    public function showLoginForm()
    {
        if(Auth::guard('nurse')->check()){
            return redirect()->route('nurse.dashboard');
        }
        else{
            return view('staff-auth.login');
        }
    }

    public function login(Request $request)
    {
        $this->validate($request,[
            'login_id' => 'required',
            'password' => 'required'
        ]);

        if(Auth::guard('nurse')->attempt(['login_id'=>$request->login_id,'password'=>$request->password],$request->remember)){
            return redirect()->route('nurse.dashboard');
        }
        else{
            // return redirect()->back()->withErrors('Invalid credentials!');
            throw ValidationException::withMessages([
                'email' => 'invalid id or password'
            ]);
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('nurse')->logout();

        $request->session()->invalidate();

        return redirect()->route('nurse.login');
    }
}
