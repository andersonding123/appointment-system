<?php

namespace App\Http\Controllers\Nurse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Models\Appointments;
use App\Models\Nurses;
use App\Models\Departments;
use App\Models\Doctors;
use PhpParser\Comment\Doc;

class AppointmentController extends Controller
{
    public function index(Request $request)
    {
        $search = "";

        if($request->isMethod('post')){
            $submit = $request->input('form-action');
            switch($submit){
                case 'submit':
                    $code = $request->input('code');
                    Session::put('search_appointment',$code);
                    break;
                case 'reset':
                    Session::forget('search_appointment');
                    break;
            }
        }

        if(Session::has('search_appointment')){
            $search = Session::get('search_appointment');
        }

        return view('nurses.appointment',[
            'records' => Appointments::get_records($search,15),
            'search' => $search
        ]);
    }

    public function assign_appointment(Request $request, $appointment_id)
    {
        $validator = null;
        $appointment = Appointments::find($appointment_id);
        if(!$appointment){
            return redirect()->route('nurse.appointment');
        }

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'doctor_id' => 'required'
            ])->setAttributeNames([
                'doctor_id' => 'Doctor'
            ]);

            if(!$validator->fails()){
                $appointment->update([
                    'doctor_id' => $request->input('doctor_id'),
                    'nurse_id' => Auth::user()->nurse_id
                ]);

                return redirect()->route('nurse.appointment');
            }
        }
        
        return view('nurses.assign_appointment',[
            'record' => $appointment,
            'available_doctors' => Doctors::get_department_doctors_sel($appointment->department_id)
        ])->withErrors($validator);
    }

    public function complete_appointment(Request $request)
    {
        $data = array('status'=>false,'msg'=>'Invalid action!');
        $appointment_id = $request->input('id');
        if(!is_numeric($appointment_id) || empty($appointment_id)){
            $data['status'] = false;
            $data['msg'] = 'Invalid data submitted!';
        }

        $appointment = Appointments::find($appointment_id);
        if(!$appointment){
            $data['status'] = false;
            $data['msg'] = 'Appointment not found!';
        }

        if($request->isMethod('post')){
            $appointment->update([
                'appointment_status' => 'done'
            ]);

            $data['status'] = true;
            $data['msg'] = 'Apppointment Updated!';
        }

        return response()->json($data,200);
    }
}
