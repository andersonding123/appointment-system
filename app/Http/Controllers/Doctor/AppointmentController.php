<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Appointments;
use App\Models\Doctors;
use Illuminate\Support\Facades\Session;

class AppointmentController extends Controller
{
    public function index(Request $request)
    {
        $doctor = Auth::user();
        $search = "";

        if($request->isMethod('post')){
            switch($request->input('form-submit')){
                case 'search':
                    $freetext = $request->input('freetext');
                    Session::put('doctor_appointment_search',$freetext);
                    break;
                case 'reset':
                    Session::forget('doctor_appointment_search');
                    break;
            }
        }

        if(Session::has('doctor_appointment_search')){
            $search = Session::get('doctor_appointment_search');
        }

        return view('doctors.appointment',[
            'records' => Appointments::get_doctor_appointment($search,$doctor->doctor_id,15),
            'search' => $search
        ]);
    }

    public function complete_appointment(Request $request)
    {
        $data = array('status'=>false,'msg'=>'Invalid action!');
        $appointment_id = $request->input('id');
        if(!is_numeric($appointment_id) || empty($appointment_id)){
            $data['status'] = false;
            $data['msg'] = 'Invalid data submitted!';
        }

        $appointment = Appointments::find($appointment_id);

        if(!$appointment){
            $data['status'] = false;
            $data['msg'] = 'Appointment not found!';
        }

        if($request->isMethod('post')){
            $appointment->update([
                'appointment_status' => 'done'
            ]);

            $data['status'] = true;
            $data['msg'] = 'Apppointment Updated!';
        }

        return response()->json($data,200);
    }
}