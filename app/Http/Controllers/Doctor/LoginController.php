<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:doctor')->except('logout');
    }

    public function showLoginForm()
    {
        if(Auth::guard('doctor')->check()){
            return redirect()->route('doctor.dashboard');
        }
        else{
            return view('staff-auth.login');
        }
    }

    public function login(Request $request)
    {
        $this->validate($request,[
            'login_id' => 'required',
            'password' => 'required'
        ]);

        if(Auth::guard('doctor')->attempt(['login_id'=>$request->login_id,'password'=>$request->password],$request->remember)){
            return redirect()->route('doctor.dashboard');
        }
        else{
            // return redirect()->back()->withErrors('Invalid credentials!');
            throw ValidationException::withMessages([
                'email' => 'invalid id or password'
            ]);
        }
    }

    public function logout(Request $request)
    {
        AUth::guard('doctor')->logout();

        $request->session()->invalidate();

        return redirect()->route('doctor.login');
    }
}
